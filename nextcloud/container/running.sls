# -*- coding: utf-8 -*-
# vim: ft=sls

{#- Get the `tplroot` from `tpldir` #}
{%- set tplroot = tpldir.split('/')[0] %}
{%- from tplroot ~ "/map.jinja" import nextcloud with context %}
{%- from tplroot ~ "/libtofs.jinja" import files_switch with context %}

{%- set mountpoint = nextcloud.docker.mountpoint %}

{%- set postgres_user_name = nextcloud.postgres.user_name %}
{%- set postgres_password = nextcloud.postgres.user_password %}

nextcloud-container-running-postgres-image-present:
  docker_image.present:
    - name: postgres
    - tag: 12-alpine
    - force: True

nextcloud-container-running-postgres-container-managed:
  docker_container.running:
    - name: postgres
    - image: postgres:12-alpine
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/postgres/db:/var/lib/postgresql/data
    - environment:
      - POSTGRES_PASSWORD: {{ postgres_password }}
      - POSTGRES_DB: nextcloud
      - POSTGRES_USER: {{ postgres_user_name }}

nextcloud-container-running-nextcloud-image-present:
  docker_image.present:
    - name: nextcloud
    - tag: 24-fpm
    - force: True

nextcloud-container-running-nextcloud-container-managed:
  docker_container.running:
    - name: nextcloud
    - image: nextcloud:24-fpm
    - restart: always
    - binds:
      - {{ mountpoint }}/docker/nextcloud/html:/var/www/html
      - {{ mountpoint }}/nextcloud/data:/var/www/html/data
      - {{ mountpoint }}/docker/nextcloud/config:/var/www/html/config
    - environment:
      - POSTGRES_HOST: postgres
      - POSTGRES_PASSWORD: {{ postgres_password }}
      - POSTGRES_DB: nextcloud
      - POSTGRES_USER: {{ postgres_user_name }}
    - links:
      - postgres: postgres
    - require:
      - nextcloud-container-running-postgres-container-managed

nextcloud-container-running-nextcloud-nginx-config-managed:
  file.managed:
    - name: {{ mountpoint }}/docker/nginx/nextcloud/nginx.conf
    - source: {{ files_switch(['nginx.conf'],
                              lookup='nextcloud-container-running-nextcloud-nginx-config-managed',
                 )
              }}
    - user: root
    - group: root
    - mode: 644
    - makedirs: True
    - require:
      - nextcloud-container-running-postgres-container-managed

nextcloud-container-running-nginx-image-present:
  docker_image.present:
    - name: nginx
    - tag: alpine
    - force: True

nextcloud-container-running-nginx-container-running:
  docker_container.running:
    - name: nextcloud-web
    - image: nginx:alpine
    - restart: always
    - port_bindings:
      - 9010:80
    - binds:
      - {{ mountpoint }}/docker/nextcloud/html:/var/www/html:ro
      - {{ mountpoint }}/docker/nginx/nextcloud/nginx.conf:/etc/nginx/nginx.conf:ro
    - links:
      - nextcloud: nextcloud
    - watch:
      - nextcloud-container-running-nextcloud-nginx-config-managed
    - labels:
      - "traefik.enable=true"
      - "traefik.http.middlewares.nextcloud-redirect-websecure.redirectscheme.scheme=https"
      - "traefik.http.routers.nextcloud-web.middlewares=nextcloud-redirect-websecure"
      - "traefik.http.routers.nextcloud-web.rule=Host(`{{ nextcloud.container.url }}`)"
      - "traefik.http.routers.nextcloud-web.entrypoints=web"
      - "traefik.http.routers.nextcloud-websecure.rule=Host(`{{ nextcloud.container.url }}`)"
      - "traefik.http.routers.nextcloud-websecure.tls.certresolver=letsencrypt"
      - "traefik.http.routers.nextcloud-websecure.tls=true"
      - "traefik.http.routers.nextcloud-websecure.entrypoints=websecure"
    - require:
      - nextcloud-container-running-nextcloud-container-managed
      - nextcloud-container-running-nextcloud-nginx-config-managed

nextcloud-container-running-nextcloud-config-trusted-proxies-added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'trusted_proxies' => array ('192.168.0.0/24'),"
    - mode: ensure
    - before: "  'datadirectory' => '/var/www/html/data',"
    - require:
      - nextcloud-container-running-nextcloud-container-managed

nextcloud-container-running-nextcloud-config-forwarded-for-headers-added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'forwarded_for_headers' => array ('HTTP_CF_CONNECTING_IP'),"
    - mode: ensure
    - after: "  'datadirectory' => '/var/www/html/data',"
    - require:
      - nextcloud-container-running-nextcloud-container-managed

nextcloud-container-running-nextcloud-config-overwrite-cli-url-added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'overwrite.cli.url' => 'https://{{ nextcloud.container.url }}',"
    - mode: ensure
    - before: "  'dbname' => 'nextcloud',"
    - require:
      - nextcloud-container-running-nextcloud-container-managed

nextcloud-container-running-nextcloud-config-overwrite-protocol-added:
  file.line:
    - name: {{ mountpoint }}/docker/nextcloud/config/config.php
    - content: "  'overwriteprotocol' => 'https',"
    - mode: ensure
    - after: "  'dbname' => 'nextcloud',"
    - require:
      - nextcloud-container-running-nextcloud-container-managed
